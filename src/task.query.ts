import * as Listr from "listr";

class TaskQuery {
  private tasks: any;
  private actions: any = {};

  constructor() {
    this.init();
  }

  init() {
    this.tasks = new Listr([
      // {
      //   title: "Connecting to databases",
      //   task: () => new Promise(resolve => setTimeout(() => resolve(true), 1000))
      // },
      // {
      //   title: "Listing database tables",
      //   task: () => this.createTask('mysqlListingTables')
      // }
    ]);
    return this;
  }

  createTask(name) {
    return new Promise((resolve, reject) => {
      this.actions[name] = (state = true) => {
        if (state) {
          resolve(state);
        } else {
          reject();
        }
      };
    })
  }

  run() {
    this.tasks.run();
    return this;
  }

}

export default new TaskQuery().run();
