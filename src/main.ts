/**
 * database-migration-tool
 */
import * as fs from 'fs';
import {ConnectDbOptions} from './drivers/db.driver';
import {MysqlDriver, MysqlDriverQueryResult} from './drivers/mysql.driver';
import {MongoDriver} from './drivers/mongo.driver';
import {TypeTranslator} from './translators/type.translator';
import {ConfigLoader} from "./config.loader";
import {logger} from "./logger.engine";

class Main {

  private mysql: MysqlDriver;
  private mongodb: MongoDriver;
  private destinationDatabase: String;

  private mysqlOptions: ConnectDbOptions;
  private mongodbOptions: ConnectDbOptions;

  private configLoader: ConfigLoader;

  constructor() {
    this.configLoader = new ConfigLoader();
    this.loadConf();
    this.mysql = new MysqlDriver(this.mysqlOptions);
    this.mongodb = new MongoDriver(this.mongodbOptions);
  }


  async run() {
    try {
      await this.mysql.createConnection();

      let mysqlSchemas = {};
      let mongoSchemas = {};

      try {
        let result: MysqlDriverQueryResult = await this.mysql.query('SHOW TABLES');

        logger.info(`Getting tables info from ${this.mysqlOptions.database.toLowerCase()} db`);

        let typeTranslator = new TypeTranslator();

        for (let res of result.results) {
          const tName = `Tables_in_${this.mysqlOptions.database.toLowerCase()}`;
          const pName = res[tName];

          mysqlSchemas[pName] = {};
          logger.info(`Processing ${pName} table`);
          let r1 = await this.mysql.query(`DESC ${pName}`);
          //console.log(pName, r1.results);
          for (let r2 of r1.results) {
            mysqlSchemas[pName][r2.Field] = typeTranslator.searchType(r2.Type);
          }

        }
        let mongoModels = {};
        let mongoDefinitions = {};

        for (let name in mysqlSchemas) {
          let translatedModelName = this.mongodb.translateModelName(name);
          const mysqlSchema = mysqlSchemas[name];

          const {model, schema, definition} = this.mongodb.mapToModel(translatedModelName, mysqlSchema);
          mongoModels[translatedModelName] = model;
          mongoSchemas[translatedModelName] = schema;
          mongoDefinitions[translatedModelName] = definition;

          logger.info(`Transferring data table ${name}`);

          let queryResult = await this.mysql.query(this.selectTableDataQuery(name));

          for (let modelData of queryResult.results) {
            let MongoModel = mongoModels[translatedModelName];
            let m = new MongoModel();

            for (let mDataFieldName in modelData) {
              // console.log("fieldType", mDataFieldName, mysqlSchema[mDataFieldName]);
              let mType = mysqlSchema[mDataFieldName];

              let dataToInsert = modelData[mDataFieldName];

              if (mType === "objectOrArray") {
                console.log("objectArray", dataToInsert);
                dataToInsert = JSON.parse(dataToInsert);
              }

              if (mType === "dateTime") {
                console.log("transform dateTime", dataToInsert);
                dataToInsert = dataToInsert !== "0000-00-00 00:00:00" ? new Date(dataToInsert) : null;
              }

              m[mDataFieldName] = dataToInsert;
            }

            await m.save();
          }
        }
        // console.log(mongoModels);
        logger.info("Migration completed successfully");
      } catch (e) {
        logger.error("QueryError", e);
      } finally {
        fs.writeFileSync("./translated.mysql.schema.json", JSON.stringify(mysqlSchemas));
        fs.writeFileSync("./mongodb.schema.json", JSON.stringify(mongoSchemas));

      }

      process.exit(0);

    } catch (e) {
      logger.error('ConnectError: ', e);
    }
  }

  selectTableDataQuery(tableName) {
    return `SELECT * FROM ${tableName}`;
  }

  private loadConf() {
    let conf = this.configLoader.getConfig();
    console.log(conf);

    this.mysqlOptions = conf.database.mysql;
    this.mongodbOptions = conf.database.mongodb;
    this.destinationDatabase = conf.destination;
  }
}

new Main().run();
