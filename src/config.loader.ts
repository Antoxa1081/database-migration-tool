import {ConnectDbOptions} from './drivers/db.driver';
import * as fs from 'fs';

class DataBaseConfig {
  public readonly mysql: ConnectDbOptions;
  public readonly mongodb: ConnectDbOptions;

  constructor(mysql: ConnectDbOptions, mongodb: ConnectDbOptions) {
    this.mysql = mysql as ConnectDbOptions;
    this.mongodb = mongodb as ConnectDbOptions;
  }

}

export class ConfigSchema {
  public readonly destination: string;
  public readonly database: DataBaseConfig;

  constructor(configData: any) {
    this.destination = configData.destination as string;
    this.database = new DataBaseConfig(configData.database.mysql, configData.database.mongodb)
  }

}

export class ConfigLoader {

  private configPath: string = './config.json';
  private configTemplatePath: string = './config.example.json';
  private config: ConfigSchema;

  constructor() {
    this.init();
  }

  init() {
    if (fs.existsSync(this.configPath)) {
      let conf = require('../config.json');
      this.config = new ConfigSchema(conf);
    } else {
      fs.copyFileSync(this.configTemplatePath, this.configPath);
      console.log('Created default config.json file, fill it');
      process.exit(0);
    }
  }

  getConfig() {
    return this.config;
  }

}
