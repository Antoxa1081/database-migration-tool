export class TypeTranslator {
  private assignTypeMap = {
    numeric: [
      'INT',
      'TINYINT',
      'SMALLINT',
      'MEDIUMINT',
      'BIGINT',
      'FLOAT',
      'DOUBLE',
      'DECIMAL',
      'TIMESTAMP',
    ],
    dateTime: [
      'DATE',
      'DATETIME',
      'TIME',
      'YEAR',
    ],
    objectOrArray: [
      'JSON'
    ],
    string: [
      "CHAR",
      "VARCHAR",
      "TEXT",
      "TINYTEXT",
      "MEDIUMTEXT",
      "LONGTEXT",
      "BLOB",
      "TINYBLOB",
      "MEDIUMBLOB",
      "LONGBLOB",
      "ENUM",
    ]
  };

  searchType(type: string): string {
    for (let srcName in this.assignTypeMap) {
      for (let destName of this.assignTypeMap[srcName]) {
        if (type.toUpperCase().includes(destName)) {
          return srcName;
        }
      }
    }
    return null;
  }

}
