import * as mysql from 'mysql';
import {Connection, ConnectionConfig, FieldInfo, QueryOptions} from 'mysql';
import {DbDriver} from "./db.driver";
import {logger} from "../logger.engine";

export interface MysqlDriverQueryResult {
  results?: any,
  fields?: FieldInfo[]
}

export class MysqlDriver extends DbDriver {

  public options: ConnectionConfig;
  private link: Connection;

  constructor(options?: ConnectionConfig) {
    super(options);
    // this.handleEvents();
  }

  setOptions(options: ConnectionConfig) {
    this.options = options;
  }

  createConnection(): Promise<Connection> {
    return new Promise<Connection>(((resolve, reject) => {
      this.link = mysql.createConnection(this.options);

      this.link.connect(err => {
        if (err) {
          reject(err);
          return;
        }

        resolve(this.link);
      });
    }));
  }

  // handleEvents(){
  //   this.link.on("error", error => logger.error(error));
  // }

  query(options: string | QueryOptions): Promise<MysqlDriverQueryResult> {
    return new Promise<MysqlDriverQueryResult>(((resolve, reject) => {
      this.link.query(options, ((err, results, fields) => {
        if (err) {
          reject(err);
          return;
        }
        resolve({
          results, fields
        });
      }))
    }));
  }

}
