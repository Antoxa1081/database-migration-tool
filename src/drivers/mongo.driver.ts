import {ConnectDbOptions, DbDriver} from "./db.driver";
import * as mongoose from "mongoose";
import {Model, model, Schema} from "mongoose";
import {logger} from "../logger.engine";

export interface mapToModelResponse {
  model: Model<any>;
  schema: Schema;
  definition: object;
}

export class MongoDriver extends DbDriver {
  private types = {
    numeric: Number,
    string: String,
    dateTime: Date,
    object: Object,
    array: Array,
    objectOrArray: Schema.Types.Mixed,
  };

  private db;

  constructor(options: ConnectDbOptions) {
    super(options);

    this.initConnection();
  }

  setOptions(options: any) {
    this.options = options;
  }

  mapToModel(name: string, map: any): mapToModelResponse {
    let definition = {};
    for (let name in map) {
      definition[name] = this.types[map[name]];
    }
    let schema: Schema = new Schema(definition);
    return {model: model(name, schema), schema, definition};
  }

  translateModelName(name: string): string {
    return `${name[0].toUpperCase()}${name.substr(1, name.length - (name[name.length - 1].toLowerCase() === "s" ? 2 : 1))}`;
  }

  private async initConnection() {
    await mongoose.connect(this.genConnectStr(), {
      useNewUrlParser: true,
      // useUnifiedTopology: true,
      reconnectTries: 1000,
      reconnectInterval: 5000,
    });

    this.db = mongoose.connection;

    this.db.on("connected", () => {
      logger.info("MongoDB connected")
      // console.log(`Mongoose connection open on ${this.genConnectStr()}`);
    });

    this.db.on("error", err => logger.error(err));

    this.db.on("disconnected", () => {
      logger.warn("MongoDB disconnected")
      // console.log("mongoose connection disconnected");
    });


  }

  private genConnectStr(): string {
    return `mongodb://${this.options.user}:${this.options.password}@${this.options.host}/${this.options.database}`;
  }
}
