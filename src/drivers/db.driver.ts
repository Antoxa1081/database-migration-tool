export interface ConnectDbOptions {
  host: string;
  user: string;
  password: string;
  database: string;
}

export abstract class DbDriver {

  constructor(public options?: any) {

  }

  abstract setOptions(options: any): void;

}
